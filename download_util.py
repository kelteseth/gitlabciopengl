import sys
import time
import ssl
import urllib.request

def progress(count, block_size, total_size):
    percent = int(count * block_size * 100 / total_size)
    sys.stdout.write("\rDownload: {}%".format(percent))
    sys.stdout.flush()

# disable broken progress bar in CI
def download(url, filename, show_progress=True):
    # This is needed for downloading from https sites
    # see https://programmerah.com/python-error-certificate-verify-failed-certificate-has-expired-40374/
    ssl._create_default_https_context = ssl._create_unverified_context

    if show_progress:
        urllib.request.urlretrieve(url, filename, progress)
    else:
        urllib.request.urlretrieve(url, filename)
        