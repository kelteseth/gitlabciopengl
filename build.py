from install_requirements import install_requirements

install_requirements()
print("")

import os
import sys
import stat
import shutil
import argparse
import py7zr
import zipfile
import download_util
from execute_util import execute


GODOT_VERSION = "3.4.4"
GODOT_RELEASE_TYPE = "stable"
LOCAL_GODOT_K3000_SUBPATH = "Tools" 



def get_godot_executable(godot_version, godot_release_type):
    if sys.platform == "win32":
        return f"Godot_v{godot_version}-{godot_release_type}_win64.exe"
    elif sys.platform == "darwin":
        return f"Godot_v{godot_version}-{godot_release_type}_osx.universal"
    elif sys.platform == "linux":
        return f"Godot_v{godot_version}-{godot_release_type}_x11.64"
    else:
        print(f"Error: Unknown platform {sys.platform}")
        return None


def download_godot(version, exe_zip_filename, export_templates, download_destination_path):
    download_export_templates = f"https://downloads.tuxfamily.org/godotengine/{version}/{export_templates}"
    exe_destination_filepath = os.path.join(download_destination_path, exe_zip_filename)
    export_templates_destination_path = os.path.join(download_destination_path, export_templates)

    # Godot adds ".stable" to the folder names for full releases: "AppData/Roaming/Godot/templates/3.4.stable":
    print("Downloading Godot from https://downloads.tuxfamily.org/godotengine/")
    download_link = f"https://downloads.tuxfamily.org/godotengine/{version}/{exe_zip_filename}"
    download_util.download(download_link, exe_destination_filepath, False)
    download_util.download(download_export_templates, export_templates_destination_path, False)

def unzip(zip_path, destination_path, specific_file=None):
    with zipfile.ZipFile(zip_path, 'r') as zip_ref:
        if specific_file:
            zip_ref.extract(specific_file, path=destination_path)
        else:
            zip_ref.extractall(path=destination_path)

def unzip_godot(exe_zip_filepath, export_templates_filepath, destination_path):
    print("Unzip Godot")
    unzip(exe_zip_filepath, destination_path)

    # The export templates contain a templates subfolder in which the content is. This is bad because it clashes
    # with the folder structure where the version comes after: AppData\Roaming\Godot\templates\3.3.4.stable
    # Rename: AppData\Roaming\Godot\templates\templates
    # to    : AppData\Roaming\Godot\templates\3.4.stable
    godot_templates_dir = os.path.join(os.getenv('APPDATA'), "Godot/templates/")
    os.makedirs(godot_templates_dir, exist_ok=True)
    export_templates_destination_version = f"{godot_templates_dir}/{GODOT_VERSION}.{GODOT_RELEASE_TYPE}"

    # Remove previous folder
    if os.path.exists(export_templates_destination_version):
        print(f"Remove previous export templates folder: {export_templates_destination_version}")
        shutil.rmtree(export_templates_destination_version)

    unzip(export_templates_filepath, godot_templates_dir)
    os.rename(os.path.join(godot_templates_dir, "templates"), export_templates_destination_version)

    print(f"Remove {exe_zip_filepath}")
    try:
        os.remove(exe_zip_filepath)
    except OSError as error:
        print(f"Error deleting file: {error}")
        return False

    return True


def generate_godot_export_presets(export_path, build_path):

    with open(os.path.join(export_path, "default_export_presets.cfg"), "r") as f:
        lines = f.readlines()

        for i, line in enumerate(lines):
            if line.startswith("export_path=\"HTML5\""):
                # replace the lines with build_path
                lines[i] = "export_path =\"" + build_path.replace("\\","/") + "/index.html\"\n"
            if line.startswith("export_path=\"Windows Desktop\""):
                # replace the lines with build_path
                lines[i] = "export_path =\"" + build_path.replace("\\","/") + "/Test.exe\"\n"

        with open(os.path.join(export_path, "export_presets.cfg"), "w") as f:
            f.writelines(lines)

def build_godot(root_path, abs_build_path):
    os.chdir(root_path)
    godot_export_path = (abs_build_path).replace("\\","/")
    if not os.path.exists(godot_export_path):
        os.makedirs(godot_export_path)

    generate_godot_export_presets(os.path.abspath(os.path.curdir),abs_build_path)

    apps_path = os.path.join(root_path,"Tools")
    godot_executable = ""

    # Get absolute godot path from "Tools"
    if sys.platform == "win32":
        godot_executable = "{}/Godot_v{}-{}_win64.exe".format(apps_path,GODOT_VERSION,GODOT_RELEASE_TYPE)
    elif sys.platform == "darwin":
        godot_executable = "Godot.app"
    elif sys.platform == "linux":
        godot_executable = "{}/Godot_v{}-{}_x11.64".format(GODOT_VERSION,GODOT_RELEASE_TYPE)
    
    if sys.platform == "win32":
        execute(("{} -v --no-window --export \"HTML5\" ").format(godot_executable,godot_export_path))
        execute(("{} -v --no-window --export \"Windows Desktop\" ").format(godot_executable,godot_export_path))
    elif sys.platform == "darwin": 
        execute(("{} -v --no-window --export \"HTML5\" {}/index.html").format(godot_executable,godot_export_path))
    elif sys.platform == "linux": 
        execute(("{} -v --no-window --export \"HTML5\" {}/index.html").format(godot_executable,godot_export_path))

def main():
    print(f"Set up GODOT version {GODOT_VERSION} {GODOT_RELEASE_TYPE}")
    executable = get_godot_executable(GODOT_VERSION, GODOT_RELEASE_TYPE)
    root_path = os.path.abspath(os.getcwd())
    local_path = os.path.join(root_path, LOCAL_GODOT_K3000_SUBPATH)
    build_path = os.path.join(root_path, "Export")
    destination_path = os.path.join(root_path, local_path)
    export_templates = f"Godot_v{GODOT_VERSION}-{GODOT_RELEASE_TYPE}_export_templates.tpz"
    export_templates_filepath = os.path.join(destination_path, export_templates)
    exe_zip_filename = executable + '.zip'
    exe_zip_filepath = os.path.join(destination_path, exe_zip_filename)

    print(f"Download GODOT")
    download_godot(GODOT_VERSION, exe_zip_filename, export_templates, destination_path)
    print(f"Unzip GODOT")
    if not unzip_godot(exe_zip_filepath, export_templates_filepath, destination_path):
        return False

    print(f"Build/Export GODOT project")
    build_godot(os.path.realpath(root_path), build_path)
    print(f"finish")
    #print(f"Build/Export GODOT project")
    #build_godot(os.path.realpath(root_path), build_path)
    #print(f"finish")

if __name__ == "__main__":
    main()