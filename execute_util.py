import subprocess


def execute(command, working_dir=".", ignore_error=False, use_shell=True):
    out = subprocess.run(
        command, stdout=subprocess.PIPE, stderr=subprocess.PIPE, shell=False, cwd=working_dir).stdout.decode('utf-8')
    print(out)
